package com.bohdan.fibonachi;

import java.util.ArrayList;

/**
 * Class builds a Fibonachi Sequence
 *
 */

public class FibonachiMaker {

    public static int fib(int n) {
        if (n <= 1) {
            return n;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }

    /**
     * Put the Fibonachi Sequence into ArrayList
     * @param size describes the size of Sequence
     * @return ArrayList integers
     */

    public ArrayList<Integer> makeFibon (int size) {
        ArrayList<Integer> fibonSequence = new ArrayList<Integer>();
        for (int i = 0; i < size; i++) {
            fibonSequence.add(fib(i));
        }
        return fibonSequence;
    }

}


