package com.bohdan.fibonachi;

import java.util.ArrayList;

/**
 * This class finds max ODD number in Fibonachi Sequence
 */

public class FibonachiCalculations {

    /**
     * Returns max ODD number
     *
     * @param fibonachi takes ArrayList Fbonachi sequence
     * @return int max ODD value
     */

    public int biggestODDmumber(ArrayList<Integer> fibonachi) {
        int max = fibonachi.get(0);
        for (int i = 0; i < fibonachi.size(); i++) {
            if (ifOdd(fibonachi.get(i))) {
                if (max < fibonachi.get(i)) {
                    max = fibonachi.get(i);
                }
            }
        }
        return max;
    }

    /**
     * Calculates haw much Odd and Even numbers in percent
     *
     * @param fibonachi takes fibonachi sequence
     */

    public void calcOddEvenPercentage(ArrayList<Integer> fibonachi) {
        double persent = 100;
        double evenNumbersAmount = 0;
        double oddNumber = 0;
        for (int i = 0; i < fibonachi.size(); i++) {
            if (!ifOdd(fibonachi.get(i))) {
                evenNumbersAmount++;
            } else {
                oddNumber++;
            }
        }
        double oddpercentage = (oddNumber / evenNumbersAmount) * persent;
        System.out.println("Odd numbers is " + oddpercentage + "%");
        double evenNumbersPerentage = persent - oddpercentage;
        System.out.println("Even numbers is " + evenNumbersPerentage + "%");
    }

    private boolean ifOdd(Integer number) {
        if (number == 0) {
            return false;
        } else if (number == 1) {
            return false;
        } else if (number % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}
