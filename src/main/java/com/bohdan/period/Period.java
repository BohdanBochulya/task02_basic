package com.bohdan.period;

public class Period {

    public String printOddnumbers(int leftPeriodBorder, int rightPeriodBorder) {
        StringBuilder result = new StringBuilder();
        while (leftPeriodBorder <= rightPeriodBorder) {
            if (ifOdd(leftPeriodBorder)) {
                result.append(leftPeriodBorder + ",");
                leftPeriodBorder++;
            } else {
                leftPeriodBorder++;
            }
        }
        return result.toString();
    }

    public String printNumbersSumm(int leftPeriodBorder, int rightPeriodBorder) {
        int summ = leftPeriodBorder;
        while (leftPeriodBorder <= rightPeriodBorder) {
            summ = summ + leftPeriodBorder;
            leftPeriodBorder++;
        }
        return Integer.toString(summ);
    }

    private boolean ifOdd(int number) {
        if (number % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}
