package com.bohdan;

import com.bohdan.fibonachi.FibonachiCalculations;
import com.bohdan.fibonachi.FibonachiMaker;
import com.bohdan.period.Period;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Application {

    /**
     * This is the main method of the application. The user just need to specify period start and end.
     * Also set the Fibonachi Sequence size.
     * @param args unused
     * @throws Exception
     */

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter the diapason start and press Enter");
        String diapasonStart = reader.readLine();
        System.out.println("Please enter the diapason start and press Enter");
        String diapasonEnd = reader.readLine();
        if (!isNumeric(diapasonStart) || !isNumeric(diapasonEnd)) {
            System.out.print("Invalid input data. Restart the application");
        }

        int leftPeriodBorder = Integer.parseInt(diapasonStart);
        int rightPeriodBorder = Integer.parseInt(diapasonEnd);

        if (leftPeriodBorder > rightPeriodBorder) {
            int temp = leftPeriodBorder;
            leftPeriodBorder = rightPeriodBorder;
            rightPeriodBorder = temp;
        }

        Period period = new Period();
        String odd = period.printOddnumbers(leftPeriodBorder, rightPeriodBorder);
        System.out.println("The odd numbers \n" + odd);
        String periodSumm = period.printNumbersSumm(leftPeriodBorder, rightPeriodBorder);
        System.out.println("The numbers summ is: " + periodSumm);

        System.out.println("Please enter the size of Fibonachi Sequence");
        String fibSize = reader.readLine();
        FibonachiMaker fibonachiMaker = new FibonachiMaker();
        ArrayList<Integer> fibonSequence = fibonachiMaker.makeFibon(Integer.parseInt(fibSize));
        System.out.println("-----------------------------------------------");
        System.out.println("Here is our Febonachi sequence");
        System.out.println(Arrays.toString(fibonSequence.toArray()));

        FibonachiCalculations fibonachi = new FibonachiCalculations();
        System.out.print("The biggest Odd number in Fibonachi sequence is:");
        System.out.println(fibonachi.biggestODDmumber(fibonSequence));
        fibonachi.calcOddEvenPercentage(fibonSequence);
    }

    /**
     * This method checks if input String is number
     * @param strNum input String parameter
     * @return returns boolean
     */

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }
}
